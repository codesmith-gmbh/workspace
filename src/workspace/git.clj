(ns workspace.git
  (:require [workspace.shell :as shell]))

(defn root []
  (shell/sh "git" "rev-parse" "--show-toplevel"))

(defn clean? []
  (= (count (shell/sh "git" "status" "-s")) 0))

(defn push-all! []
  (shell/sh! "git" "push")
  (shell/sh! "git" "push" "--tags"))

(defn branch []
  (shell/sh ))

(defn push! []
  (shell/sh! ))

(comment

  )