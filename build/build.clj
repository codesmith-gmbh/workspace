(ns build
  (:require [ch.codesmith.anvil.libs :as libs]
            [ch.codesmith.anvil.release :as rel]
            [ch.codesmith.anvil.shell :as sh]
            [clojure.tools.build.api :as b]))

(def lib 'ch.codesmith.workspace/workspace)
(def version (str "0." (b/git-count-revs {}) "." (or (System/getenv "WS_RELEASE") "0-SNAPSHOT")))
(def release-branch-name "main")

(def description-data
  {:license        :epl
   :inception-year 2020
   :description    "A CLI tool for workspaces"
   :organization   {:name "Codesmith GmbH"
                    :url  "https://codesmith.ch"}
   :authors        [{:name  "Stanislas Nanchen"
                     :email "stan@codesmith.ch"}]
   :scm            {:type         :gitlab
                    :organization "codesmith-gmbh"
                    :project      "blocks"}})

(defn verify [_]
  (sh/sh! "./build/verify"))

(defn release "library with git releases"
  [_]
  (rel/git-release! {:deps-coords         lib
                     :version             version
                     :release-branch-name release-branch-name
                     :artifact-type       :deps})
  (verify nil))
